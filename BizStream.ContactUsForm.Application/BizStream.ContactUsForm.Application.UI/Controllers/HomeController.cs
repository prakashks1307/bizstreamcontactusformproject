﻿using BizStream.ContactUsForm.Application.BusinessLogic.Interfaces;
using BizStream.ContactUsForm.Application.BusinessLogic.Models;
using BizStream.ContactUsForm.Application.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BizStream.ContactUsForm.Application.UI.Controllers
{
    public class HomeController : Controller
    {

        private IContactUsFormManager formManager;

        public HomeController(IContactUsFormManager formManager)
        {
            this.formManager = formManager;
        }

        public ActionResult Index()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ContactUsFormViewModel model)
        {
            if (!ModelState.IsValid)
                return View();
            try
            {
                var fileName = ConfigurationManager.AppSettings["FileName"];
                formManager.SaveForm(new ContactUsFormBusinessModel()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    Message = model.Message,
                    FavoriteColor = model.FavoriteColor,
                    IsHuman = model.IsHuman
                }, Server.MapPath("~") + fileName);
                ViewBag.ResultMessage = "Form saved";
            }
            catch (Exception ex)
            {
                ViewBag.ResultMessage = "Form failed to save due to " + ex.Message;
            }
            ViewBag.Message = "Your contact page.";
            ModelState.Clear();
            return View();
        }
    }
}