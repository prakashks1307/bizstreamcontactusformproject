using BizStream.ContactUsForm.Application.BusinessLogic.Interfaces;
using BizStream.ContactUsForm.Application.BusinessLogic.Managers;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace BizStream.ContactUsForm.Application.UI
{
    public static class UnityConfig
    {
        public static void RegisterTypes()
        {
			var container = new UnityContainer();
            container.RegisterType<IContactUsFormManager, ContactUsFormManager>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}