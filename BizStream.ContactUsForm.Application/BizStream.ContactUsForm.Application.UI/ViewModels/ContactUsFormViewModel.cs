﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BizStream.ContactUsForm.Application.UI.ViewModels
{
    public class ContactUsFormViewModel
    {
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [DisplayName("Email")]
        [Required(ErrorMessage = "Email field is required.")]
        [EmailAddress(ErrorMessage = "This is not a valid email address")]
        public string Email { get; set; }

        [DisplayName("Message")]
        public string Message { get; set; }

        [DisplayName("Favorite Color")]
        public string FavoriteColor { get; set; }

        [DisplayName("Is Human?")]
        public bool IsHuman { get; set; }

    }
}