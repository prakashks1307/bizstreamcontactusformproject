﻿using BizStream.ContactUsForm.Application.BusinessLogic.Interfaces;
using BizStream.ContactUsForm.Application.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BizStream.ContactUsForm.Application.BusinessLogic.Managers
{
    public class ContactUsFormManager : IContactUsFormManager
    {
        /// <summary>
        /// Save the give data to a text file.
        /// </summary>
        /// <param name="model">Data to be saved in text file.</param>
        /// <param name="filePath">Path of the text file.</param>
        public void SaveForm(ContactUsFormBusinessModel model, string filePath)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append($"First Name: {model.FirstName}");
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append($"Last Name: {model.LastName}");
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append($"Email: {model.Email}");
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append($"Message: {model.Message}");
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append($"Favorite Color: {model.FavoriteColor}");
            stringBuilder.Append(Environment.NewLine);
            var isHuman = model.IsHuman ? "Yes" : "No";
            stringBuilder.Append($"Is Human: {isHuman}");
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append("----------------------------------------------------------------------------------------------------");
            stringBuilder.Append(Environment.NewLine);
            var formData = stringBuilder.ToString();

            using (var stream = File.Open(filePath, FileMode.OpenOrCreate))
            {
                stream.Seek(0, SeekOrigin.End);
                var bytes = new System.Text.UnicodeEncoding().GetBytes(formData);
                stream.Write(bytes, 0, bytes.Length);
            };
        }
    }
}
