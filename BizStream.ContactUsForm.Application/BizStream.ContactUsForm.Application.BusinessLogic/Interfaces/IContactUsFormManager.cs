﻿using BizStream.ContactUsForm.Application.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BizStream.ContactUsForm.Application.BusinessLogic.Interfaces
{
    public interface IContactUsFormManager
    {
        void SaveForm(ContactUsFormBusinessModel model, string fileName);
    }
}
